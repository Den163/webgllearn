import * as path from "path";
import * as webpack from "webpack";

// const ClosureCompilerPlugin = require("webpack-closure-compiler"); 

declare var __dirname: string;

const config: webpack.Configuration = {
    entry: "./src/main.ts",
    output: {
        path: path.resolve(__dirname, "public"),
        filename: "main.js"
    },

    resolve: {
        extensions: [ ".ts", ".tsx", ".js", ".vertex" ]
    },

    module: {
        rules: [
            { test: /\.tsx?$/, use: "awesome-typescript-loader", exclude: /node_modules/ },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },

    plugins: [
    ],
    
    externals: {
        query: "jquery"
    },

    watch: true
};

export default config;
