import { GLBufferArray } from './WebGLController/GLRenderer/GLStructures/GLBufferArray';
import { vec3, vec4, vec2 } from 'gl-matrix';
import WebGLController from "./WebGLController/WebGLController";

export class Program {
    public static Main(): void {
        this.InitGL();
    }

    public static InitGL(): void {
        const canvas = document.getElementById("canvas") as HTMLCanvasElement;
        const glController = new WebGLController(canvas);      

        const objectFactory = glController.objectFactory;
        const rect1 = objectFactory.CreateRect(0.0, 0.0, 1, 1, -0.1);
        const rect2 = objectFactory.CreateRect(0.0, 0.0, 1, 1, -0.2);
        const rect3 = objectFactory.CreateRect(0.0, 0.0, 1, 1, 0.0);
        const rotation = vec3.fromValues(0.0, 0.0, 0.0);

        rect1.texture.LoadTexture("images/sprite.png");

        rect2.mesh.bufferArray.SetVertexColor(1, vec4.fromValues(0.0, 1.0, 0.0, 1.0));

        const rotationVec = vec3.create();

        glController.renderer.camera.LookAt(vec3.fromValues(0.2, 0.5, 0.5), vec3.create());

        glController.onupdate = () => {
            /*
            rect.transform.rotation = vec3.add(rotationVec, 
                rect.transform.rotation, rotation); */
        };
    }

    public static Test(): void {
        const a = new GLBufferArray();
        a.AddVertex( vec3.fromValues(0.0, 0.5, 0.0) );
        a.AddVertex( vec3.fromValues(0.0, 0.5, 0.0) );
        a.AddVertex( vec3.fromValues(0.0, 0.5, 0.0) );

        const SIZE = 1000;
        const verteces = new Float32Array(Math.pow(3, 10));

        for (let i = 0; i < verteces.length; i++) {
            verteces[i] = Math.random();
        }

        a.AddVerteces(verteces);

        a.SetVertexColor(2, vec4.fromValues(0.0, 1.0, 1.0, 1.0));
    }
}
