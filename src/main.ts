import * as $ from "jquery";
import { Program } from './Program';
import "es6-promise/auto";

(window as any).$ = $;

$(document).ready(
    () => {
        Program.Main();
    }
);
