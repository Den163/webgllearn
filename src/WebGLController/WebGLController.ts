import { GLObjectFactory } from './GLRenderer/GLObjectFactory';
import { GLRenderer } from './GLRenderer/GLRenderer';
import { GLTransform } from './GLRenderer/GLObject/Components/GLTransform';
import { GLObject } from './GLRenderer/GLObject/GLObject';
import { vec3, vec4, vec2 } from 'gl-matrix';
import { GLBuilder } from './GLRenderer/GLBuilder';
import { GLBufferArray } from './GLRenderer/GLStructures/GLBufferArray';

export default class WebGLController {
    private glRenderer: GLRenderer;

    public get objectFactory(): GLObjectFactory { return this.glRenderer.objectFactory; }
    public get renderer(): GLRenderer { return this.glRenderer; }
    public set onupdate(updateFunc: () => void) { this.glRenderer.onupdate = updateFunc; }

    constructor(canvas: HTMLCanvasElement) {
        this.InitGLRenderer(canvas);
    }

    private InitGLRenderer(canvas: HTMLCanvasElement) {
        const glBuilder = new GLBuilder(canvas);
        this.glRenderer = glBuilder.BuildGLRenderer("shaders/vertex.vert", "shaders/fragment.frag");

        this.glRenderer.Init(["a_Position", "a_PointSize", "a_Color", "a_TexCoord"], 
            ["u_TransformMatrix", "u_ViewMatrix", "u_Sampler0"]);
    }
}
