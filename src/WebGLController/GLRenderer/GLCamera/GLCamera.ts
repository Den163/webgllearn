import { glMatrix, mat4, vec3 } from 'gl-matrix';

const toRad = glMatrix.toRadian;

export class GLCamera {

    private viewMatrix: mat4;
    private gl: WebGLRenderingContext;
    private u_ViewMatrix: WebGLUniformLocation;
    private upVector: vec3;

    /**
     *  Creates a camera object
     */
    constructor(gl: WebGLRenderingContext, u_ViewMatrix: WebGLUniformLocation) {
        this.gl = gl;
        this.u_ViewMatrix = u_ViewMatrix;

        this.viewMatrix = mat4.create();
        this.upVector = vec3.fromValues(0.0, 1.0, 0.0);

        this.UpdateUniform();
    }

    public LookAt(cameraPosition: vec3, objectPosition: vec3)
    {
        mat4.lookAt(this.viewMatrix, cameraPosition, objectPosition, this.upVector);

        this.UpdateUniform();
    }

    public Rotate(rotation: vec3)
    {
        vec3.rotateX(this.upVector, this.upVector, this.upVector, toRad(rotation[0]));
        vec3.rotateY(this.upVector, this.upVector, this.upVector, toRad(rotation[1]));
        vec3.rotateZ(this.upVector, this.upVector, this.upVector, toRad(rotation[2]));
        
        this.UpdateUniform();
    }

    public UpdateUniform(): void {
        const gl = this.gl;

        gl.uniformMatrix4fv(this.u_ViewMatrix, false, this.viewMatrix);
    }
}
