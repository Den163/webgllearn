import * as glm from "gl-matrix";
import { GLRenderer } from './GLRenderer';

export class GLBuilder {
    private canvas: HTMLCanvasElement;
    private gl: WebGLRenderingContext;
    private shaderProgram: WebGLProgram;

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
    }

    public BuildGLRenderer(vertexShaderFilePath: string, fragmentShaderFilePath: string): GLRenderer {
        this.BuildGLContext();
        this.BuildShaderProgram(vertexShaderFilePath, fragmentShaderFilePath);
        
        const glProgram = new GLRenderer(this.gl, this.shaderProgram);
        return glProgram;
    }

    private BuildGLContext(): void {
        const canvas = this.canvas;

        const gl = canvas.getContext("webgl") as WebGLRenderingContext || 
            canvas.getContext("experimental-webgl") as WebGLRenderingContext;
        
        if (!gl) {
            throw new Error("Can't retrieve WebGL context");
        }

        this.gl = gl;
    }

    private BuildShaderProgram(vertexShaderFilePath: string, fragmentShaderFilePath: string): void {
        const gl = this.gl;
        const vertexShader = this.CompileShader(gl.VERTEX_SHADER, vertexShaderFilePath);
        const fragmentShader = this.CompileShader(gl.FRAGMENT_SHADER, fragmentShaderFilePath);

        this.LinkShaderProgram(vertexShader, fragmentShader);
    }

    private CompileShader(shaderType: number, sourceFile: string): WebGLShader {
        const gl = this.gl;
        const shader = gl.createShader(shaderType);
        const source = this.GetShaderSource(sourceFile);

        if (!shader) {
            throw new Error("Can't create shader");
        }

        gl.shaderSource(shader, source);
        gl.compileShader(shader);
        
        const status = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
        if (!status) {
            throw new Error(`Failed to compile shader: 
                ${gl.getShaderInfoLog(shader)}`);
        }

        return shader;
    }

    private LinkShaderProgram(vertexShader: WebGLShader, fragmentShader: WebGLShader): void {
        const gl = this.gl;
        
        const shaderProgram = gl.createProgram();

        if (!shaderProgram) {
            throw new Error("Failed to create shader program");
        }
        
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        const status = gl.getProgramParameter(shaderProgram, gl.LINK_STATUS);
        if (!status) {
            throw new Error(`Failed to link shader program: 
                ${gl.getProgramInfoLog(shaderProgram)}`);
        }

        gl.useProgram(shaderProgram);
        
        this.shaderProgram = shaderProgram;
    }

    private GetShaderSource(sourceFile: string): string {
        let sourceText = "";
        
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = () => {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                sourceText = xhttp.responseText;
            }
        };

        xhttp.open("GET", sourceFile, false);
        xhttp.send();

        return sourceText;
    }
}
