import { vec2, vec3, vec4 } from 'gl-matrix';

const DEFAULT_POINT_SIZE = 10.0;
const FLOAT_SIZE = Float32Array.BYTES_PER_ELEMENT;
const INDEX_OF_VERTICES = 0;
const INDEX_OF_POINT_SIZE = 3;
const INDEX_OF_COLOR = 4;
const INDEX_OF_TEXTURE = 8;

const ROW_SIZE: number = 10;

const COLOR_OFFSET: number = FLOAT_SIZE * INDEX_OF_COLOR;
const POINT_SIZE_OFFSET: number = FLOAT_SIZE * INDEX_OF_POINT_SIZE;
const BYTES_IN_ROW: number = FLOAT_SIZE * ROW_SIZE;
const VERTICES_OFFSET: number = FLOAT_SIZE * INDEX_OF_VERTICES;
const TEXTURE_OFFSET: number = FLOAT_SIZE * INDEX_OF_TEXTURE;

const ELEMENTS_IN_ROW_WITHOUT_VERTICES = ROW_SIZE - 3; // 3 components (x,y,z)

/**
 * This data structure contains all information about 
 * vertices and their sizes, colors, uv coordinates, etc
 */
export class GLBufferArray {
    private meshInfo: Float32Array;
    private elements: number;

    public get colorOffset() { return COLOR_OFFSET; }
    public get pointSizeOffset() { return POINT_SIZE_OFFSET; }
    public get rowBytes() { return BYTES_IN_ROW; }
    public get textureOffset() { return TEXTURE_OFFSET; }
    public get verticesOffset() { return VERTICES_OFFSET; }

    public get verticesCount() { return this.elements / ROW_SIZE; }

    /**
     * 
     * @param size Size of buffer array
     */
    constructor(size?: number) {
        size = size || 32;

        this.elements = 0;
        this.meshInfo = new Float32Array(size);
    }

    /**
     * Returns an array representation of the data structure 
     * (That you can bind for example to the buffer object)
     */
    public GetArray(): Float32Array {
        return this.meshInfo;
    }

    /**
     * Adds vertex to the data structure
     * @param vertex Vertex position that you want to add
     */
    public AddVertex(vertex: vec3): void {
        const addedElements = this.elements + vertex.length
            + ELEMENTS_IN_ROW_WITHOUT_VERTICES;

        if (addedElements > this.meshInfo.length) {
            this.Resize(addedElements);
        }

        vertex.forEach(
            (coordinate) => { this.meshInfo[this.elements++] = coordinate; }
        );

        this.meshInfo[this.elements] = DEFAULT_POINT_SIZE;

        this.elements += ELEMENTS_IN_ROW_WITHOUT_VERTICES;
    }

    /**
     * Add multiple verteces. Note that it uses Float32Array
     * @param verteces Typed Array with sequence [x1, y1, z1, x2, y2, z2, ..., xn, yn, zn]
     */
    public AddVerteces(verteces: Float32Array): void {
        if (verteces.length % 3 !== 0) {
            throw new Error("Verteces array need to contain 3 components(x,y,z)");
        }

        const addedVertexes = verteces.length / 3; // 3 components (x,y,z)
        const addedElements = this.elements + verteces.length
             + ELEMENTS_IN_ROW_WITHOUT_VERTICES * addedVertexes;

        if (addedElements > this.meshInfo.length) {
            this.Resize(addedElements);
        }

        let i = 0;
        while (i < verteces.length) {
            this.AddVertex(vec3.fromValues(
                verteces[i++], verteces[i++], verteces[i++]));
        }
    }

    /**
     * Changes color of the given vertex
     * @param vertexIndex Vertex index (count starts from 0) that you want to change
     * @param color Color (r,g,b,a) that you want to add to vertex
     */
    public SetVertexColor(vertexIndex: number, color: vec4) {
        this.CheckIsVertexOutOfRange(vertexIndex);

        let colorElementIndex = vertexIndex * ROW_SIZE + INDEX_OF_COLOR;

        color.forEach(
            (colorComponent) => {
                this.meshInfo[colorElementIndex++] = colorComponent;
            }
        );
    }

    /**
     * Changes point size of the given vertex (Works only if you use gl.POINTS draw mode)
     * @param vertexIndex Vertex index (count starts from 0) that you want to change
     * @param size Size that you want to set
     */
    public SetPointSize(vertexIndex: number, size: number) {
        this.CheckIsVertexOutOfRange(vertexIndex);

        const pointSizeElementIndex = vertexIndex * ROW_SIZE + INDEX_OF_POINT_SIZE;

        this.meshInfo[pointSizeElementIndex] = size;
    }

    /**
     * Changes UV texture coordinates of given vertex
     * @param vertexIndex Vertex index (count starts from 0) that you want to change
     * @param uv UV coordinates of texture that you want to set
     */
    public SetVertexUV(vertexIndex: number, uv: vec2) {
        this.CheckIsVertexOutOfRange(vertexIndex);

        const uvElementIndex = vertexIndex * ROW_SIZE + INDEX_OF_TEXTURE;

        this.meshInfo[uvElementIndex] = uv[0];
        this.meshInfo[uvElementIndex + 1] = uv[1];
    }

    public PrintContent() {
        let str = "";

        for (let i = 0; i < this.meshInfo.length; i++) {
            str += this.meshInfo[i] + ", ";

            if ((i + 1) % ROW_SIZE === 0) {
                str += "\n";
            }
        }

        console.log(str);
    }

    private CheckIsVertexOutOfRange(vertexIndex: number): void {
        const verticesCount = this.meshInfo.length / ROW_SIZE;

        if (vertexIndex > verticesCount - 1) {
            throw new Error(`Vertex index: ${vertexIndex} is out of range`);
        }
    }

    private Resize(size: number) {
        const oldMeshInfo = this.meshInfo;
        
        this.meshInfo = new Float32Array(size);
        oldMeshInfo.forEach(
            (value, index) => { this.meshInfo[index] = value; }
        );
    }
}
