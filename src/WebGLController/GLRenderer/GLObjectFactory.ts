import { vec2, vec4 } from 'gl-matrix';
import { GLObject } from './GLObject/GLObject';
import { GLRenderer } from './GLRenderer';

const DEFAULT_COLOR = vec4.fromValues(1.0, 1.0, 1.0, 1.0); 

export class GLObjectFactory {
    private gl: WebGLRenderingContext;
    private glRenderer: GLRenderer;
    private objectContainer: GLObject[];

    constructor(glRenderer: GLRenderer, objectContainer: GLObject[]) {
        this.glRenderer = glRenderer;
        this.objectContainer = objectContainer;
        this.gl = this.glRenderer.gl;
    }

    public CreateRect(centerX: number, centerY: number, 
                      width: number, height: number, z?: number): GLObject {
        z = z || 0.0;                        
        const halfWidht = width * 0.5;
        const halfHeight = height * 0.5;        
        const verteces = new Float32Array([
            centerX - halfWidht, centerY - halfHeight, z,
            centerX - halfWidht, centerY + halfHeight, z,
            centerX + halfWidht, centerY + halfHeight, z,
            centerX + halfWidht, centerY - halfHeight, z
        ]);
        
        const object = new GLObject(this.glRenderer, this.gl.TRIANGLE_FAN, verteces);
        const bufArray = object.mesh.bufferArray;

        bufArray.SetVertexUV(1, vec2.fromValues(0.0, 1.0));
        bufArray.SetVertexUV(2, vec2.fromValues(1.0, 1.0));
        bufArray.SetVertexUV(3, vec2.fromValues(1.0, 0.0));

        for (let i = 0; i < 4; i++) {
            bufArray.SetVertexColor(i, DEFAULT_COLOR);
        }

        this.objectContainer.push(object);

        return object;
    }

    public CreateCircle(centerX: number, centerY: number, 
                        radius: number, z?: number): GLObject {
        z = z || 0.0;
        
        const vertecesCount = 128;
        const verteces = new Float32Array(vertecesCount * 3);
        const ANGLE_STEP = 2 * Math.PI / (vertecesCount - 3);
        let angle = 0;
        verteces[0] = centerX;
        verteces[1] = centerY;
        
        for (let i = 3; i < verteces.length - 3; i += 3) {
            verteces[i] = centerX + Math.cos(angle) * radius;
            verteces[i + 1] = centerY + Math.sin(angle) * radius;
            angle += ANGLE_STEP;
        }

        verteces[verteces.length - 3] = verteces[0];
        verteces[verteces.length - 2] = verteces[1];

        const object = new GLObject(this.glRenderer, this.gl.TRIANGLE_FAN, verteces);
        this.objectContainer.push(object);

        return object;
    }
}
