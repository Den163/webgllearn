import { GLTexture } from './GLTexture';
import { GLRenderer } from './../../GLRenderer';
import { GLBufferArray } from './../../GLStructures/GLBufferArray';

export class GLMesh {
    private __bufferArray: GLBufferArray;
    private vertexBuffer: WebGLBuffer;
    private glRenderer: GLRenderer;
    private gl: WebGLRenderingContext;

    public get verticesCount() { return this.__bufferArray.verticesCount; }
    public get bufferArray() { return this.__bufferArray; }

    constructor(glRenderer: GLRenderer, verteces: Float32Array, texture?: GLTexture) {
        this.glRenderer = glRenderer;
        this.gl = glRenderer.gl;

        this.InitGLBufferArray(verteces);
        this.InitWebGlBufferObject();
    }

    public Update() {
        this.WriteDataInBuffer();
    }

    private InitGLBufferArray(verteces: Float32Array): void {
        this.__bufferArray = new GLBufferArray();        
        this.__bufferArray.AddVerteces(verteces);
    }

    private InitWebGlBufferObject(): void {
        const gl = this.gl;        

        this.vertexBuffer = this.vertexBuffer || gl.createBuffer();
        if (!this.vertexBuffer) {
            throw new Error("Failed to create vertex buffer object");
        }
        
        this.WriteDataInBuffer();
    }

    private WriteDataInBuffer() {
        const gl = this.gl;
        this.vertexBuffer = gl.createBuffer() || this.vertexBuffer;
        const bufferArray = this.__bufferArray.GetArray();        
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, bufferArray, gl.STATIC_DRAW);

        this.InitVertexBuffer();
        this.InitPointSizeBuffer();
        this.InitColorBuffer();
        this.InitTextureBuffer();
    }

    private InitVertexBuffer(): void {
        const gl = this.gl;
        const a_Position = this.glRenderer.GetAttribute("a_Position");
        const stride = this.__bufferArray.rowBytes;
        const vertexOffset = this.__bufferArray.verticesOffset;

        gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, stride, vertexOffset);
        gl.enableVertexAttribArray(a_Position);
    }

    private InitPointSizeBuffer(): void {
        const gl = this.gl;
        const a_PointSize = this.glRenderer.GetAttribute("a_PointSize");

        const pointOffset = this.__bufferArray.pointSizeOffset;        
        const stride = this.__bufferArray.rowBytes;
        
        gl.vertexAttribPointer(a_PointSize, 1, gl.FLOAT, false, stride, pointOffset);
        gl.enableVertexAttribArray(a_PointSize);
    }

    private InitColorBuffer(): void {
        const gl = this.gl;        
        const a_Color = this.glRenderer.GetAttribute("a_Color");
        
        const colorOffset = this.__bufferArray.colorOffset;
        const stride = this.__bufferArray.rowBytes;
        
        gl.vertexAttribPointer(a_Color, 4, gl.FLOAT, false, stride, colorOffset);
        gl.enableVertexAttribArray(a_Color);
    }

    private InitTextureBuffer(): void {
        const gl = this.gl;
        const a_TexCoord = this.glRenderer.GetAttribute("a_TexCoord");

        const textureOffset = this.__bufferArray.textureOffset;
        const stride = this.__bufferArray.rowBytes;

        gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, stride, textureOffset);
        gl.enableVertexAttribArray(a_TexCoord);
    }
}
