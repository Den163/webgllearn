import { GLRenderer } from './../../GLRenderer';
export class GLTexture {
    private image: HTMLImageElement;
    private glRenderer: GLRenderer;

    public get imageTexture() { return this.image; }

    constructor(glRenderer: GLRenderer, imagePath?: string);
    constructor(glRenderer: GLRenderer, image?: HTMLImageElement);
    constructor(glRenderer: GLRenderer, image?: string | HTMLImageElement) {
        this.glRenderer = glRenderer;

        if (typeof image === "string") {
            this.LoadTexture(image);
        } else
        if (image instanceof HTMLImageElement) {
            this.image = image;
        }

        this.InitDefaultTexture();
    }

    public async LoadTexture(imageSrc: string, textureSlot: number = 0) {
        await this.AsyncLoadImage(imageSrc).catch(
            (err) => { alert(err.message); }
        );
        this.InitTexture(textureSlot);
    }

    private AsyncLoadImage(imageSrc: string) {
        const image = new Image();

        return new Promise((resolve, reject) => {
            image.onload = (ev) => {
                this.image = image;
                resolve();
            };

            image.onerror = (ev) => {
                reject(new Error("Failed to load image: " + imageSrc));
            };

            setTimeout( () => { 
                reject(new Error("Time's out. Failed to retrieve image from server")); 
            }, 10000 );

            image.src = imageSrc; 
        });        
    }

    private InitTexture(textureSlot: number) {
        const gl = this.glRenderer.gl;
        const u_Sampler = this.glRenderer.GetUniform("u_Sampler" + textureSlot);   
        const texture = gl.createTexture();             
        
        if (!texture) {
            throw new Error("Failer to create new texture");
        }

        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

        gl.activeTexture(this.GetTextureSlotEnum(textureSlot));
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image);

        gl.uniform1i(u_Sampler, textureSlot);
    }

    private InitDefaultTexture() {
        const gl = this.glRenderer.gl;
        const texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);

        // Fill the texture with a 1x1 blue pixel.
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
                new Uint8Array([0, 0, 255, 255]));
    }

    private GetTextureSlotEnum(textureSlot: number): number {
        const gl = this.glRenderer.gl;        
        const TEXTURE_MIN = 0;
        const TEXTURE_MAX = 7;

        if (textureSlot < TEXTURE_MIN || textureSlot > TEXTURE_MAX) 
            throw new Error(`Texture slot must be from ${TEXTURE_MIN} to ${TEXTURE_MAX}`);

        return (gl as any)["TEXTURE" + textureSlot];
    }
}
