import { glMatrix, mat4, vec3, quat } from "gl-matrix";

const toRad = glMatrix.toRadian;

export class GLTransform {
    private gl: WebGLRenderingContext;
    private transformMatrix: mat4;
    private u_TransformMatrix: WebGLUniformLocation;

    private _position : vec3;
    private _localPosition : vec3;
    private _rotation : vec3; 
    private _localRotation : vec3;
    private _scale : vec3; 
    private _localScale : vec3;

    public get position() : vec3 { return this._position; }
    public set position(v : vec3) { this._position = v; this.UpdateUniform(); }
    public get localPosition() : vec3 { return this._localPosition; }
    public set localPosition(v : vec3) { this._localPosition = v; this.UpdateUniform(); }
    public get rotation() : vec3 { return this._rotation; }
    public set rotation(v : vec3) { this._rotation = v; this.UpdateUniform(); }
    public get localRotation() : vec3 { return this._localRotation; }
    public set localRotation(v : vec3) { this._localRotation = v; this.UpdateUniform(); }
    public get scale() : vec3 { return this._scale; }
    public set scale(v : vec3) { this._scale = v; this.UpdateUniform(); }
    public get localScale() : vec3 { return this._localScale; }
    public set localScale(v : vec3) { this._localScale = v; this.UpdateUniform(); }

    constructor(gl: WebGLRenderingContext, u_TransformMatrix: WebGLUniformLocation) {
        this.u_TransformMatrix = u_TransformMatrix;
        this.gl = gl;

        this._localPosition = vec3.create();
        this._position = vec3.create();
        this._localRotation = vec3.create();
        this._rotation = vec3.create();
        this._localScale = vec3.fromValues(1.0, 1.0, 1.0);
        this._scale = vec3.fromValues(1.0, 1.0, 1.0);

        this.UpdateUniform();
    }
    
    private UpdateUniform(): void {
        const gl = this.gl;
        this.transformMatrix = mat4.create();

        this.ScaleTransform( this._scale );
        this.RotateTransform(this._rotation);
        this.TranslateTransform(this._position);
        this.ScaleTransform(this._localScale);
        this.TranslateTransform(this._localPosition);
        this.RotateTransform(this._localRotation);

        gl.uniformMatrix4fv(this.u_TransformMatrix, false, this.transformMatrix);
    }
        
    private RotateTransform(rotationVec: vec3): void {
        mat4.rotateX(this.transformMatrix, this.transformMatrix, toRad(rotationVec[0]));
        mat4.rotateY(this.transformMatrix, this.transformMatrix, toRad(rotationVec[1]));
        mat4.rotateZ(this.transformMatrix, this.transformMatrix, toRad(rotationVec[2]));
    }
    
    private TranslateTransform(translationVector: vec3): void {
        mat4.translate(this.transformMatrix, this.transformMatrix, translationVector);
    }

    private ScaleTransform(scaleVector: vec3) {
        mat4.scale(this.transformMatrix, this.transformMatrix, scaleVector);
    }
}
