import { GLTexture } from './Components/GLTexture';
import { mat4, vec3, vec4 } from "gl-matrix";
import { GLBufferArray } from './../GLStructures/GLBufferArray';
import { GLMesh } from './Components/GLMesh';
import { GLRenderer } from '../GLRenderer';
import { GLTransform } from './Components/GLTransform';

export enum DrawMode {
    Points = WebGLRenderingContext.POINTS,
    Triangles = WebGLRenderingContext.TRIANGLES,
    Triangles_Fan = WebGLRenderingContext.TRIANGLE_FAN,
    Triangles_Strip = WebGLRenderingContext.TRIANGLE_STRIP
}

export class GLObject {
    private glRenderer: GLRenderer;
    private drawMode: DrawMode;
    
    private __mesh: GLMesh;
    private __texture: GLTexture;  
    private __transform: GLTransform;
    
    public get mesh(): GLMesh { return this.__mesh; }
    public get texture(): GLTexture { return this.__texture; }
    public get transform(): GLTransform { return this.__transform; }    

    constructor(glRenderer: GLRenderer, drawMode: DrawMode, verteces: Float32Array) {
        if ((verteces.length % 3) !== 0 ) {
            throw new Error("vertices parameter must contain 3 components(x, y, z)");
        }

        this.drawMode = drawMode;
        this.glRenderer = glRenderer;
        
        this.__mesh = new GLMesh(this.glRenderer, verteces);
        this.__texture = new GLTexture(glRenderer);

        // this.InitUniform();
        this.InitTransform();
        this.Draw();
    }

    private InitTransform(): void {
        const gl = this.glRenderer.gl;
        const u_TransformMatrix = this.glRenderer.GetUniform("u_TransformMatrix");

        this.__transform = new GLTransform(gl, u_TransformMatrix);
    }
    
    private InitUniform(): void {
        const gl = this.glRenderer.gl;

        const u_Width = this.glRenderer.GetUniform("u_Width");
        const u_Height = this.glRenderer.GetUniform("u_Height");
        const canvas = document.getElementById("canvas") as HTMLCanvasElement;
        
        gl.uniform1f(u_Width, canvas.width);
        gl.uniform1f(u_Height, canvas.height);  
    }

    public Draw(): void {
        const gl = this.glRenderer.gl;
        this.__mesh.Update();

        gl.drawArrays(this.drawMode, 0, this.__mesh.verticesCount);
    }
}
