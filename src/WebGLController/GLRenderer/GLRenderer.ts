import { GLCamera } from './GLCamera/GLCamera';
import { GLObjectFactory } from './GLObjectFactory';
import { GLObject } from './GLObject/GLObject';
import { IHashTable } from './Interfaces/IHashTable';

export class GLRenderer {
    private __gl: WebGLRenderingContext;
    private __objectFactory: GLObjectFactory;
    private __camera: GLCamera;

    private shaderProgram: WebGLProgram;
    private objectContainer: GLObject[];
    private attributes: IHashTable<number>;
    private uniforms: IHashTable<WebGLUniformLocation>;

    private updateFunc(): void {}

    public get gl() { return this.__gl; }
    public get objectFactory() { return this.__objectFactory; }
    public get camera() { return this.__camera; }
    public set onupdate(updFunc: () => void) { this.updateFunc = updFunc;  }

    constructor(gl: WebGLRenderingContext, shaderProgram: WebGLProgram) {
        this.__gl = gl;
        this.shaderProgram = shaderProgram;

        this.objectContainer = [];
        this.__objectFactory = new GLObjectFactory(this, this.objectContainer);
        
        this.attributes = {};
        this.uniforms = {};
        
        this.Clear();
    }
    
    public Init(attributesNames: string[], uniformsNames: string[]) {        
        this.InitAttributes(attributesNames);
        this.InitUniforms(uniformsNames);
        this.__camera = new GLCamera(this.__gl, this.GetUniform("u_ViewMatrix"));        

        this.Draw();
    }

    public GetAttribute(name: string): number {
        const attr = this.attributes[name];
        
        if (attr < 0) {
            throw new Error(`There is no attribute ${name} in the shader program`);
        }
        
        return attr;
    }

    public GetUniform(name: string): WebGLUniformLocation {
        const uniform = this.uniforms[name];
        
        if (!uniform) {
            throw new Error(`There is no uniform ${name} in the shader program`);
        }
        
        return uniform;
    }

    private Draw(): void {
        this.Clear();

        const gl = this.__gl;
        const FRAME_SPEED = 1000 / 60;   

        const drawFunc = () => {
            gl.clear(gl.COLOR_BUFFER_BIT);
            
            this.updateFunc();
            
            this.objectContainer.forEach(
                (glObject) => { glObject.Draw(); }
            );
        };
        
        setInterval(drawFunc, FRAME_SPEED);
    }
    
    private Clear(): void {
        const gl = this.__gl;

        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT);
    }
    
    private InitAttributes(attributesNames: string[]): void {
        for (const attribName of attributesNames) {
            const attribIndex = this.__gl.getAttribLocation(this.shaderProgram, attribName);
            
            if (attribIndex < 0) {
                throw new Error(`There is no ${attribName} attribute in the shader source`);
            }
            
            this.attributes[attribName] = attribIndex;
        }
    }
    
    private InitUniforms(uniformsNames: string[]): void {
        for (const uniformName of uniformsNames) {
            const uniformVar = this.__gl.getUniformLocation(this.shaderProgram, uniformName);
            
            if (!uniformVar) {
                throw new Error(`There is no ${uniformName} uninform in the shader source`);
            }
            
            this.uniforms[uniformName] = uniformVar;
        }
    }
}
