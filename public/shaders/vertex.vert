attribute vec4 a_Position;
attribute float a_PointSize;
attribute vec2 a_TexCoord;
attribute vec4 a_Color;

varying vec4 v_Color;
varying vec2 v_TexCoord;

#define M_PI 3.1415926

float angle = M_PI;
uniform mat4 u_TransformMatrix;
uniform mat4 u_ViewMatrix;

void main() {
    gl_Position =  u_ViewMatrix * u_TransformMatrix * a_Position;
    gl_PointSize = a_PointSize;

    v_Color = a_Color;
    v_TexCoord = a_TexCoord;
}
