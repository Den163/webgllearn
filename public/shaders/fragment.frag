precision mediump float;
uniform sampler2D u_Sampler0;
uniform sampler2D u_Sampler1;
uniform sampler2D u_Sampler2;

varying vec2 v_TexCoord;
varying vec4 v_Color;

void main() {
    gl_FragColor = texture2D(u_Sampler0, v_TexCoord) * v_Color;
}